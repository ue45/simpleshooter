﻿// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

namespace MG
{
	/// Convert the given value in centimeters to Unreal Units (uu).
	/// The function assumes 1uu=1cm.
	constexpr float Centimeters(const float Centimeters) noexcept
	{
		return Centimeters;
	}

	/// Convert the given value in meters to Unreal Units (uu).
	/// The function assumes 1uu=1cm.
	constexpr float Meters(const float Meters) noexcept
	{
		return Meters * 100;
	}

	/// Convert the given value in kilometers to Unreal Units (uu).
	/// The function assumes 1uu=1cm.
	constexpr float Kilometers(const float Kilometers) noexcept
	{
		return Kilometers * 1000 * 100;
	}
}

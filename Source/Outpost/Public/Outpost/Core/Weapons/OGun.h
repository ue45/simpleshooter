// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"
#include "GameFramework/Actor.h"

#include "OGun.generated.h"

class USkeletalMeshComponent;
class UParticleSystem;
class USoundBase;

// TODO: https://en.wikipedia.org/wiki/Gun#Types

UENUM()
enum class EGunState
{
    Idle,
    Firing,
    Equipping,
    Reloading
};

UCLASS()
class OUTPOST_API AOGun: public AActor
{
    GENERATED_BODY()

public:
    AOGun();

public:
    void PullTrigger();
    void Reload();

public:
    UFUNCTION(BlueprintPure, Category = "Gun|Ammo")
    int32 GetCurrentAmmo() const;

protected:
    bool GunTrace(FHitResult &Hit, FVector &ShotDirection);

protected:
    // Components ------------------------------------------------------------------------------------------------------

    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Components")
    USkeletalMeshComponent *MeshComp;

    // Gun Capabilities ------------------------------------------------------------------------------------------------

    /** The maximum range of the projectile fired from the gun. */
    UPROPERTY(EditDefaultsOnly, Category = "Gun Capabilities")
    float MaxRange = 3000;

    /** The default damage caused by the projectile fired from the gun. */
    UPROPERTY(EditDefaultsOnly, Category = "Gun Capabilities")
    float Damage = 10;

    /** The ammo capacity of the gun. Zero or negative means the gun has infinite ammo. */
    UPROPERTY(EditDefaultsOnly, Category = "Gun Capabilities")
    int32 MaxAmmo = 10;

    /** The initial amount of ammo. The value will be clipped if it is greater than Max Ammo. */
    UPROPERTY(EditDefaultsOnly, Category = "Gun Capabilities")
    int32 InitialAmmo = 10;

    // Effects ---------------------------------------------------------------------------------------------------------

    /** The visual effect to show near the muzzle when the gun fires. */
    UPROPERTY(EditDefaultsOnly, Category = "Effects")
    UParticleSystem *MuzzleFlash;

    UPROPERTY(EditDefaultsOnly, Category = "Effects")
    UParticleSystem *HitParticle;

    // Sounds ----------------------------------------------------------------------------------------------------------

    /** The sound to play when the gun fires. */
    UPROPERTY(EditDefaultsOnly, Category = "Sounds")
    USoundBase *MuzzleSound;

    /** The sound to play when the gun's projectile hits an obstacle. */
    UPROPERTY(EditDefaultsOnly, Category = "Sounds")
    USoundBase *ImpactSound;

    /** The sound to play when trying to fire the gun that has no ammo. */
    UPROPERTY(EditDefaultsOnly, Category = "Sounds")
    USoundBase *OutOfAmmoSound;

    /** The sound to play when reloading the gun. */
    UPROPERTY(EditDefaultsOnly, Category = "Sounds")
    USoundBase *ReloadSound;

private:
    AController *GetOwnerController() const;
    int32        CurrentAmmo;
};

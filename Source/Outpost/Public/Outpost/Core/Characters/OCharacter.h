// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "OCharacter.generated.h"

class AOGun;

UCLASS()
class OUTPOST_API AOCharacter: public ACharacter
{
    GENERATED_BODY()

public:
    AOCharacter();

public:
    void Tick(float DeltaTime) override;

    float TakeDamage(
        float DamageAmount, const struct FDamageEvent &DamageEvent, class AController *EventInstigator,
        AActor *DamageCauser) override;

public:
    UFUNCTION(BlueprintPure, Category = "Health")
    bool IsDead() const;

    UFUNCTION(BlueprintPure, Category = "Health")
    float GetHealthPercent() const;

protected:
    void         BeginPlay() override;
    virtual void Kill();

protected:
    UPROPERTY(EditDefaultsOnly, Category = "Weapon")
    TSubclassOf<AOGun> GunClass;

    UPROPERTY(EditDefaultsOnly, Category = "Health")
    float MaxHealth = 100.f;

    UPROPERTY(EditAnywhere, Category = "Health")
    float Health;

    UPROPERTY()
    AOGun *Gun;
};

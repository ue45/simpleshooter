// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Outpost/Core/Characters/OCharacter.h"

#include "OPlayerCharacter.generated.h"

UCLASS()
class OUTPOST_API AOPlayerCharacter: public AOCharacter
{
    GENERATED_BODY()

public:
    virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

    void MoveForwardBackward(float AxisValue);
    void MoveLeftRight(float AxisValue);
    void LookUpDown(float AxisValue);
    void LookLeftRight(float AxisValue);
    void Shoot();
    void Heal();
    void StartRunning();
    void StopRunning();

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float RotationRate = 10.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float WalkingSpeed = 300.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float RunningSpeed = 600.f;
};

// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "OPlayerController.generated.h"

class UUserWidget;

UCLASS()
class OUTPOST_API AOPlayerController: public APlayerController
{
    GENERATED_BODY()

public:
    void GameHasEnded(AActor *EndGameFocus, bool bIsWinner) override;

protected:
    void BeginPlay() override;

private:
    void ShowEndGameScreen(TSubclassOf<UUserWidget> ScreenClass);

private:
    UPROPERTY(EditAnywhere)
    float RestartDelay = 5.f;

    UPROPERTY(EditAnywhere)
    TSubclassOf<UUserWidget> GameLostScreenClass;

    UPROPERTY(EditAnywhere)
    TSubclassOf<UUserWidget> GameWonScreenClass;

    UPROPERTY(EditAnywhere)
    TSubclassOf<UUserWidget> HUDClass;

private:
    FTimerHandle RestartTimer;

    UPROPERTY()
    UUserWidget *HUD;
};

// Copyright (C) 2021 Molfar Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "OGameMode.generated.h"

UCLASS()
class OUTPOST_API AOGameMode: public AGameModeBase
{
    GENERATED_BODY()

public:
    virtual void PawnKilled(APawn *Pawn);
};

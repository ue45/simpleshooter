// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"

#include "Outpost/Core/GameModes/OGameMode.h"

#include "KillEmAllGameMode.generated.h"

UCLASS()
class OUTPOST_API AKillEmAllGameMode: public AOGameMode
{
    GENERATED_BODY()

public:
    void PawnKilled(APawn *Pawn) override;

private:
    void EndGame(bool bIsPlayerWinner);

    bool AllEnemiesDead() const;
};

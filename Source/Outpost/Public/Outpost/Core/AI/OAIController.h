// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"

#include "OAIController.generated.h"

class UBehaviorTree;

UCLASS()
class OUTPOST_API AOAIController: public AAIController
{
    GENERATED_BODY()

public:
    bool IsDead() const;

protected:
    virtual void BeginPlay() override;

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
    UBehaviorTree *AIBehavior;
};

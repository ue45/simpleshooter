// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"

#include "BTTask_ClearBlackboardValue.generated.h"

UCLASS()
class OUTPOST_API UBTTask_ClearBlackboardValue: public UBTTask_BlackboardBase
{
    GENERATED_BODY()

public:
    UBTTask_ClearBlackboardValue();

protected:
    EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent &OwnerComp, uint8 *NodeMemory) override;
};

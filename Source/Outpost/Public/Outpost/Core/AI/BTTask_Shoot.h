// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"

#include "BTTask_Shoot.generated.h"

UCLASS()
class OUTPOST_API UBTTask_Shoot: public UBTTaskNode
{
    GENERATED_BODY()

public:
    UBTTask_Shoot();
    EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent &OwnerComp, uint8 *NodeMemory) override;
};

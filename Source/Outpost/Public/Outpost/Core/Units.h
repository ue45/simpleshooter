// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

constexpr float operator"" _cm(long double Centimeters) noexcept
{
    return Centimeters; // 1uu == 1cm
}

constexpr float operator"" _m(long double Meters) noexcept
{
    return Meters * 100;
}

constexpr float operator"" _km(long double Kilometers) noexcept
{
    return Kilometers * 1000 * 100;
}

constexpr float operator"" _cm(unsigned long long int Centimeters) noexcept
{
    return Centimeters; // 1uu == 1cm
}

constexpr float operator"" _m(unsigned long long int Meters) noexcept
{
    return Meters * 100;
}

constexpr float operator"" _km(unsigned long long int Kilometers) noexcept
{
    return Kilometers * 1000 * 100;
}

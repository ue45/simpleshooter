// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Items/MGUsableActor.h"

#include "MGPickableActor.generated.h"

UCLASS(ABSTRACT)
class OUTPOST_API AMGPickableActor: public AMGUsableActor
{
    GENERATED_BODY()

    bool bIsActive = false;

    // -----------------------------------------------------------------------------------------------------------------
    // Pickup

    /** If true, this item will spawn automatically during BeginPlay. */
    UPROPERTY(EditDefaultsOnly, Category = "Pickup")
    bool bAutoSpawn = true;

	/**
	 * If true, this item will respawn automatically once picked up.
     * The respawn delay will be chosen randomly between Respawn Delay Min and Respawn Delay Max.
	 */
    UPROPERTY(EditDefaultsOnly, Category = "Pickup")
    bool bIsRespawnable = true;

    /**
     * The minimum delay in seconds between respawns. It is ignored if the item is not respawnable.
     * The respawn delay will be chosen randomly between Respawn Delay Min and Respawn Delay Max.
     */
    UPROPERTY(EditDefaultsOnly, Category = "Pickup", meta = (EditCondition = "bIsRespawnable", ClampMin = "0.0"))
    float RespawnDelayMin = DefaultRespawnDelayMin;

    /**
     * The maximum delay in seconds between respawns. It is ignored if the item is not respawnable.
     * The respawn delay will be chosen randomly between Respawn Delay Min and Respawn Delay Max.
     */
    UPROPERTY(EditDefaultsOnly, Category = "Pickup", meta = (EditCondition = "bIsRespawnable", ClampMin = "0.0"))
    float RespawnDelayMax = DefaultRespawnDelayMax;

    // -----------------------------------------------------------------------------------------------------------------
    // Sound

    /** The sound to play when this item is picked up. */
    UPROPERTY(EditDefaultsOnly, Category = "Sounds")
    USoundBase *PickupSound = nullptr;

public:
    /** Check if the item spawns automatically during BeginPlay. */
    UFUNCTION(BlueprintPure, Category = "Pickup")
    bool IsAutoSpawnable() const;

    /** Check whether the item is allowed to respawn. */
    UFUNCTION(BlueprintPure, Category = "Pickup")
    bool IsRespawnable() const;

    /**
     * Get minimum delay in seconds between respawns. The value is ignored if the item is not respawnable.
     * The respawn delay will be chosen randomly between GetRespawnDelayMin() and GetRespawnDelayMax().
     */
    UFUNCTION(BlueprintPure, Category = "Pickup")
    float GetRespawnDelayMin() const;

    /**
     * Get maximum delay in seconds between respawns. The value is ignored if the item is not respawnable.
     * The respawn delay will be chosen randomly between GetRespawnDelayMin() and GetRespawnDelayMax().
     */
    UFUNCTION(BlueprintPure, Category = "Pickup")
    float GetRespawnDelayMax() const;

    /**
     * Check whether the item can respawn instantly.
     * An item is instantly respawnable if it is allowed to respawn and both min and max delay are zero.
     */
    UFUNCTION(BlueprintPure, Category = "Pickup")
    bool IsInstantlyRespawnable() const;

    /** Check whether the item is spawned. */
    UFUNCTION(BlueprintPure, Category = "Pickup")
    bool IsActive() const;

public:
    void PostInitializeComponents() override;

    void OnUsed(APawn *InstigatorPawn) override;

protected:
    AMGPickableActor();

    void BeginPlay() override;

    void SpawnPickup();

    void RespawnPickup();

    virtual void OnPickedUp();

    virtual void OnSpawned();

private:
    void SetMeshVisibility(bool bIsMeshVisible);

private:
    static constexpr float DefaultRespawnDelayMin = 5.f;
    static constexpr float DefaultRespawnDelayMax = 10.f;
};

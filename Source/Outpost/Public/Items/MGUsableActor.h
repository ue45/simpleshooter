// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "MGUsableActor.generated.h"

class UStaticMeshComponent;

UCLASS(ABSTRACT)
class OUTPOST_API AMGUsableActor: public AActor
{
    GENERATED_BODY()

    /** A mesh that represents this object in the game world. */
    UPROPERTY(VisibleAnywhere, Category = "Components")
    UStaticMeshComponent *MeshComp;

public:
    /** Called when a player starts looking at this actor. */
    virtual void OnGainFocus();

    /** Called when a player is no longer looking at this actor. */
    virtual void OnLostFocus();

    /** Called when a player interacts with the actor. */
    virtual void OnUsed(APawn *InstigatorPawn);

protected:
    AMGUsableActor();

    FORCEINLINE UStaticMeshComponent *GetMeshComponent() const
    {
        return MeshComp;
    }
};

// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "MGGunClip.generated.h"

class AMGGun;

UCLASS()
class OUTPOST_API AMGGunClip : public AActor
{
	GENERATED_BODY()

	// -----------------------------------------------------------------------------------------------------------------
	// Capabilities

	UPROPERTY(EditDefaultsOnly, Category = "Capabilities")
	FText Description;

	/** The maximum amount of ammo the clip can have. */
	UPROPERTY(EditDefaultsOnly, Category = "Capabilities", meta = (ClampMin = "1"))
	int32 Capacity;

	/** The current amount of ammo in the clip. */
	UPROPERTY(EditDefaultsOnly, Category = "Capabilities", meta = (ClampMin = "0"))
	int32 Ammo;

	/** The list of gun classes that can use ammo from this clip. */
	UPROPERTY(EditDefaultsOnly, Category = "Capabilities")
	TSet<TSubclassOf<AMGGun>> CompatibleGunClasses;

public:
	/** Get the maximum amount of ammo the clip can have. */
	UFUNCTION(BlueprintPure, Category = "Ammo")
	int32 GetCapacity() const;

	/** Get the current amount of ammo in the clip. */
	UFUNCTION(BlueprintPure, Category = "Ammo")
	int32 GetAmmo() const;

	/** Check whether the clip has any ammo. */
	UFUNCTION(BlueprintPure, Category = "Ammo")
	bool HasAmmo() const;

	/** Use the ammo from the clip, decreasing the current ammo by the given Amount. */
	UFUNCTION(BlueprintCallable, Category = "Ammo")
	void UseAmmo(int32 Amount = 1);

	/** Add the given Amount of ammo to the clip respecting its capacity. */
	UFUNCTION(BlueprintCallable, Category = "Ammo")
	void GiveAmmo(int32 Amount);

	AMGGunClip();

	virtual void PostInitializeComponents() override;
};

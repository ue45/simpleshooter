// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Styling/SlateBrush.h"

#include "MGUnits.h"

#include "MGGun.generated.h"

class USkeletalMeshComponent;
class USoundBase;
class UParticleSystem;

UENUM()
enum class EMGGunState
{
	Idle,
	Firing,
	Reloading
};

UCLASS(ABSTRACT)
class OUTPOST_API AMGGun : public AActor
{
	GENERATED_BODY()

	static constexpr float MinimumMaximumRange          = MG::Meters(1);
	static constexpr float DefaultMaximumRange          = MG::Kilometers(1);
	static constexpr float DefaultMaximumEffectiveRange = MG::Meters(800);
	static constexpr float DefaultDamage                = 10;
	static constexpr int32 DefaultAmmoCapacity          = 10;
	static constexpr int32 InfiniteAmmo                 = -1;

	// The current amount of ammo in the gun.
	int32 AmmoAmount;

	// The current gun state.
	EMGGunState GunState;

	// Validate UPROPERTY members and reset them to safe defaults if they are not correct.
	void ValidateAndRepair();

	// ----------------------------------------------------------
	// Specifications
	// ----------------------------------------------------------

	// TODO: ShotsPerMinute
	// TODO: ReloadDuration

	/// The model name of this gun.
	UPROPERTY(EditDefaultsOnly, Category = "Gun|Specifications")
	FText ModelName;

	/// The description of the gun model.
	UPROPERTY(EditDefaultsOnly, Category = "Gun|Specifications")
	FText ModelDescription;

	/// The greatest distance a weapon can fire without consideration of dispersion.
	/// Cannot be zero or negative. Cannot be less than Maximum Effective Range.
	UPROPERTY(EditDefaultsOnly, Category = "Gun|Specifications")
	float MaximumRange;

	/// The maximum distance at which a gun may be expected to be accurate and achieve the desired effect.
	/// Cannot be zero or negative. Cannot be greater than Maximum Range.
	UPROPERTY(EditDefaultsOnly, Category = "Gun|Specifications")
	float MaximumEffectiveRange;

	/// The default damage caused by the projectile fired from the gun.
	/// A negative value is valid and means this is a healing gun.
	/// Zero, however, is invalid as it has no logical meaning for a gun.
	UPROPERTY(EditDefaultsOnly, Category = "Gun|Specifications")
	float Damage;

	/// The maximum amount of ammo you can put into this gun.
	/// A negative or zero ammo capacity makes the gun having infinite amount of ammo and no need in reloading.
	UPROPERTY(EditDefaultsOnly, Category = "Gun|Specifications")
	int32 AmmoCapacity;

	// ----------------------------------------------------------
	// Effects
	// ----------------------------------------------------------

	/// The name of the gun muzzle mesh socket. The actor uses it for spawning shot effects.
	UPROPERTY(EditDefaultsOnly, Category = "Gun|Effects")
	FName MuzzleSocketName;

	/// The visual effect to show near the muzzle when the gun fires.
	UPROPERTY(EditDefaultsOnly, Category = "Gun|Effects")
	UParticleSystem *MuzzleEffect;

	// ----------------------------------------------------------
	// Sounds
	// ----------------------------------------------------------

	/// The sound to play when the gun fires.
	UPROPERTY(EditDefaultsOnly, Category = "Gun|Sounds")
	USoundBase *FireSound;

	/// The sound to play when trying to fire the gun that has no ammo.
	UPROPERTY(EditDefaultsOnly, Category = "Gun|Sounds")
	USoundBase *OutOfAmmoSound;

	/// The sound to play when reloading the gun.
	UPROPERTY(EditDefaultsOnly, Category = "Gun|Sounds")
	USoundBase *ReloadSound;

	// ----------------------------------------------------------
	// HUD
	// ----------------------------------------------------------

	/// The icon used by HUD to display the gun in the inventory, currently selected weapon, etc.
	UPROPERTY(EditDefaultsOnly, Category = "Gun|HUD")
	FSlateBrush IconBrush;

	// ----------------------------------------------------------
	// Components
	// ----------------------------------------------------------

	/// The mesh that represents this gun in the game world.
	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	USkeletalMeshComponent *MeshComp;

protected:
	AMGGun();

public:
	virtual void PostInitializeComponents() override;

	void Fire();

	// ----------------------------------------------------------
	// Ammo management
	// ----------------------------------------------------------

	/// Get the current amount of ammo in the gun.
	/// The negative value means the gun has infinite ammo.
	UFUNCTION(BlueprintPure, Category = "Gun|Ammo")
	int32 GetCurrentAmmo() const;

	/// Check whether the gun has infinite ammo.
	UFUNCTION(BlueprintPure, Category = "Gun|Ammo")
	bool HasInfiniteAmmo() const;

	/// Check whether the gun is fully loaded with ammo.
	UFUNCTION(BlueprintPure, Category = "Gun|Ammo")
	bool IsFullyLoaded() const;

	/// Check whether the gun has any ammo.
	UFUNCTION(BlueprintPure, Category = "Gun|Ammo")
	bool HasAmmo() const;

	/// Check whether the gun has no ammo.
	UFUNCTION(BlueprintPure, Category = "Gun|Ammo")
	bool HasNoAmmo() const;

	/// Remove the given Amount of ammo from the gun respecting its capacity. 
	UFUNCTION(BlueprintCallable, Category = "Gun|Ammo")
	void RemoveAmmo(const int32 Amount = 1);

	/// Put the given amount of ammo to the gun respecting its capacity.
	/// The function does not play reload effects. Use Reload function if you need this functionality.
	UFUNCTION(BlueprintCallable, Category = "Gun|Ammo")
	void InsertAmmo(const int32 Amount);

	/// Reload the gun trying to insert as much ammo as possible from the given Amount.
	/// The function returns the remaining Amount of ammo after reload.
	/// The function plays gun reloading effects.
	///
	/// The typical use is initialising Amount with the total amount of ammo in the inventory, then call this
	/// function and use its return value to update the total amount of ammo in the inventory.
	UFUNCTION(BlueprintCallable, Category = "Gun|Ammo")
	int32 Reload(const int32 Amount);

	// ----------------------------------------------------------
	// Specifications
	// ----------------------------------------------------------

	/// Get model name of the gun.
	UFUNCTION(BlueprintPure, Category = "Gun|Specifications")
	FText GetModelName() const;

	/// Get the description of the gun model.
	UFUNCTION(BlueprintPure, Category = "Gun|Specifications")
	FText GetModelDescription() const;

	/// Get the maximum amount of ammo the gun can have.
	/// The negative or zero value means the gun has infinite capacity.
	UFUNCTION(BlueprintPure, Category = "Gun|Specifications")
	int32 GetAmmoCapacity() const;

	/// Get the damage of the gun.
	UFUNCTION(BlueprintPure, Category = "Gun|Specifications")
	float GetDamage() const;

	/// Get the greatest distance a weapon can fire without consideration of dispersion.
	UFUNCTION(BlueprintPure, Category = "Gun|Specifications")
	float GetMaximumRange() const;

	/// Get the maximum distance at which a gun may be expected to be accurate and achieve the desired effect.
	UFUNCTION(BlueprintPure, Category = "Gun|Specifications")
	float GetMaximumEffectiveRange() const;
};

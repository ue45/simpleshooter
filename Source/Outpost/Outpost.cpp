// Copyright (C) 2021 Molfar Games, Inc. All Rights Reserved.

#include "Outpost.h"

#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, Outpost, "Outpost");

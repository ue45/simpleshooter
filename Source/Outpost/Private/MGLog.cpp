// Copyright (C) 2021 Molfar Games. All rights reserved.

#include "MGLog.h"

DEFINE_LOG_CATEGORY(LogMGTemp);
DEFINE_LOG_CATEGORY(LogMGWeapon);

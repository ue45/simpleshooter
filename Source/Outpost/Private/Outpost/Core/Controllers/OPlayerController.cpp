// Copyright (C) 2021 Molfar Games. All rights reserved.

#include "Outpost/Core/Controllers/OPlayerController.h"

#include "Blueprint/UserWidget.h"
#include "TimerManager.h"

void AOPlayerController::BeginPlay()
{
    Super::BeginPlay();

    HUD = CreateWidget(this, HUDClass);
    if (HUD) {
        HUD->AddToViewport();
    }
}

void AOPlayerController::GameHasEnded(AActor *EndGameFocus, bool bIsWinner)
{
    Super::GameHasEnded(EndGameFocus, bIsWinner);

    ShowEndGameScreen(bIsWinner ? GameWonScreenClass : GameLostScreenClass);

    GetWorldTimerManager().SetTimer(RestartTimer, this, &APlayerController::RestartLevel, RestartDelay);
}

void AOPlayerController::ShowEndGameScreen(TSubclassOf<UUserWidget> ScreenClass)
{
    if (HUD) {
        HUD->RemoveFromViewport();
    }

    UUserWidget *Screen = CreateWidget(this, ScreenClass);
    if (Screen) {
        Screen->AddToViewport();
    } else {
        UE_LOG(LogTemp, Warning, TEXT("Failed to create an end game screen"));
    }
}

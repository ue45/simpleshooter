// Copyright (C) 2021 Molfar Games. All rights reserved.

#include "Outpost/Core/Weapons/OGun.h"

#include "Components/SceneComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/EngineTypes.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Pawn.h"
#include "Kismet/GameplayStatics.h"
#include "Styling/SlateBrush.h"

AOGun::AOGun()
{
    MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));

    SetRootComponent(MeshComp);
}

void AOGun::PullTrigger()
{
    AController *OwnerController = GetOwnerController();
    if (!OwnerController) {
        return;
    }

    UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, MeshComp, TEXT("MuzzleFlashSocket"));
    UGameplayStatics::SpawnSoundAttached(MuzzleSound, MeshComp, TEXT("MuzzleFlashSocket"));

    FHitResult Hit;
    FVector    ShotDirection;
    if (GunTrace(Hit, ShotDirection)) {
        UGameplayStatics::SpawnEmitterAtLocation(this, HitParticle, Hit.Location, ShotDirection.Rotation());
        UGameplayStatics::PlaySoundAtLocation(this, ImpactSound, Hit.Location);

        FPointDamageEvent DamageEvent{Damage, Hit, ShotDirection, nullptr};
        AActor           *DamagedActor = Hit.GetActor();
        if (DamagedActor) {
            DamagedActor->TakeDamage(Damage, DamageEvent, OwnerController, this);
        }
    }
}

void AOGun::Reload()
{
}

bool AOGun::GunTrace(FHitResult &Hit, FVector &ShotDirection)
{
    AController *OwnerController = GetOwnerController();
    if (!OwnerController) {
        return false;
    }

    FVector  Location;
    FRotator Rotation;
    OwnerController->GetPlayerViewPoint(Location, Rotation);

    FVector End = Location + Rotation.Vector() * MaxRange;

    FCollisionQueryParams QueryParams;
    QueryParams.AddIgnoredActor(this);
    QueryParams.AddIgnoredActor(GetOwner());

    if (GetWorld()->LineTraceSingleByChannel(Hit, Location, End, ECC_GameTraceChannel1, QueryParams)) {
        ShotDirection = -Rotation.Vector();

        return true;
    }

    return false;
}

AController *AOGun::GetOwnerController() const
{
    auto *OwnerPawn = Cast<APawn>(GetOwner());
    if (!OwnerPawn) {
        return nullptr;
    }

    return OwnerPawn->GetController();
}

int32 AOGun::GetCurrentAmmo() const
{
    return CurrentAmmo;
}

// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

#include "Logging/LogMacros.h"

DECLARE_LOG_CATEGORY_EXTERN(LogWeapon, All, All);

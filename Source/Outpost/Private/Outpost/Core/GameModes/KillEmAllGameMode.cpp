// Copyright (C) 2021 Molfar Games. All rights reserved.

#include "Outpost/Core/GameModes/KillEmAllGameMode.h"
#include "Outpost/Core/AI/OAIController.h"

#include "EngineUtils.h"

void AKillEmAllGameMode::PawnKilled(APawn *Pawn)
{
    UE_LOG(LogTemp, Warning, TEXT("Pawn '%s' killed"), *Pawn->GetName());

    auto *PlayerController = Cast<APlayerController>(Pawn->GetController());
    if (PlayerController) {
        EndGame(false);
    } else if (AllEnemiesDead()) {
        EndGame(true);
    }
}

void AKillEmAllGameMode::EndGame(bool bIsPlayerWinner)
{
    for (AController *Controller: TActorRange<AController>(GetWorld())) {
        bool bIsWinner = Controller->IsPlayerController() == bIsPlayerWinner;
        Controller->GameHasEnded(Controller->GetPawn(), bIsWinner);
    }
}

bool AKillEmAllGameMode::AllEnemiesDead() const
{
    bool bIsAnyEnemyAlive = false;
    for (AOAIController *AIController: TActorRange<AOAIController>(GetWorld())) {
        if (!AIController->IsDead()) {
            bIsAnyEnemyAlive = true;
            break;
        }
    }

    return !bIsAnyEnemyAlive;
}

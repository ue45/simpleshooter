// Copyright (C) 2021 Molfar Games. All rights reserved.

#include "Outpost/Core/AI/BTTask_Shoot.h"
#include "Outpost/Core/Characters/OPlayerCharacter.h"

#include "AIController.h"

UBTTask_Shoot::UBTTask_Shoot()
{
    NodeName = TEXT("Shoot");
}

EBTNodeResult::Type UBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent &OwnerComp, uint8 *NodeMemory)
{
    AAIController *Controller = OwnerComp.GetAIOwner();
    if (!Controller) {
        return EBTNodeResult::Failed;
    }

    auto *Pawn = Cast<AOPlayerCharacter>(Controller->GetPawn());
    if (!Pawn) {
        return EBTNodeResult::Failed;
    }

    Pawn->Shoot();

    return EBTNodeResult::Succeeded;
}

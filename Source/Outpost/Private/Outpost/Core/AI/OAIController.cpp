// Copyright (C) 2021 Molfar Games. All rights reserved.

#include "Outpost/Core/AI/OAIController.h"
#include "Outpost/Core/Characters/OCharacter.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"

void AOAIController::BeginPlay()
{
    Super::BeginPlay();

    if (AIBehavior) {
        RunBehaviorTree(AIBehavior);
        GetBlackboardComponent()->SetValueAsVector(TEXT("StartLocation"), GetPawn()->GetActorLocation());
    }
}

bool AOAIController::IsDead() const
{
    auto *ControlledCharacter = Cast<AOCharacter>(GetCharacter());
    if (ControlledCharacter) {
        return ControlledCharacter->IsDead();
    }

    return true;
}

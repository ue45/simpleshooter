// Copyright (C) 2021 Molfar Games. All rights reserved.

#include "Outpost/Core/Characters/OCharacter.h"
#include "Outpost/Core/GameModes/OGameMode.h"
#include "Outpost/Core/Weapons/OGun.h"

#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "CoreGlobals.h"

AOCharacter::AOCharacter()
{
    PrimaryActorTick.bCanEverTick = true;
}

void AOCharacter::BeginPlay()
{
    Super::BeginPlay();

    Health = MaxHealth;

    // Hide default gun
    GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);

    Gun = GetWorld()->SpawnActor<AOGun>(GunClass);
    Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("GunSocket"));
    Gun->SetOwner(this);
}

void AOCharacter::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

float AOCharacter::TakeDamage(
    float DamageAmount, const struct FDamageEvent &DamageEvent, class AController *EventInstigator,
    AActor *DamageCauser)
{
    DamageAmount          = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
    const float NewHealth = Health - DamageAmount;
    float       DamageApplied;

    if (NewHealth < 0) {
        DamageApplied = Health;
        Health        = 0;
    } else if (NewHealth > MaxHealth) {
        DamageApplied = Health - MaxHealth;
        Health        = MaxHealth;
    } else {
        DamageApplied = DamageAmount;
        Health        = NewHealth;
    }

    UE_LOG(
        LogTemp, Warning, TEXT("Actor '%s' has taken %f points of damage; new health is %f points"), *GetName(),
        DamageApplied, Health);

    if (IsDead()) {
        Kill();
    }

    return DamageApplied;
}

bool AOCharacter::IsDead() const
{
    return FMath::IsNearlyZero(Health);
}

void AOCharacter::Kill()
{
    Health = 0;
    GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

    AOGameMode *GameMode = GetWorld()->GetAuthGameMode<AOGameMode>();
    if (GameMode) {
        GameMode->PawnKilled(this);
    }

    DetachFromControllerPendingDestroy();
}

float AOCharacter::GetHealthPercent() const
{
    return Health / MaxHealth;
}

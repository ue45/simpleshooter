// Copyright (C) 2021 Molfar Games. All rights reserved.

#include "Outpost/Core/Characters/OPlayerCharacter.h"
#include "Outpost/Core/Weapons/OGun.h"

#include "Components/InputComponent.h"
#include "Engine/EngineBaseTypes.h"
#include "GameFramework/CharacterMovementComponent.h"

void AOPlayerCharacter::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &ACharacter::Jump);
    PlayerInputComponent->BindAction(TEXT("Shoot"), EInputEvent::IE_Pressed, this, &AOPlayerCharacter::Shoot);
    PlayerInputComponent->BindAction(TEXT("Run"), EInputEvent::IE_Pressed, this, &AOPlayerCharacter::StartRunning);
    PlayerInputComponent->BindAction(TEXT("Run"), EInputEvent::IE_Released, this, &AOPlayerCharacter::StopRunning);

    PlayerInputComponent->BindAxis(TEXT("MoveForwardBackward"), this, &AOPlayerCharacter::MoveForwardBackward);
    PlayerInputComponent->BindAxis(TEXT("MoveLeftRight"), this, &AOPlayerCharacter::MoveLeftRight);
    PlayerInputComponent->BindAxis(TEXT("LookUpDown"), this, &AOPlayerCharacter::LookUpDown);
    PlayerInputComponent->BindAxis(TEXT("LookLeftRight"), this, &AOPlayerCharacter::LookLeftRight);
}

void AOPlayerCharacter::MoveForwardBackward(float AxisValue)
{
    AddMovementInput(GetActorForwardVector() * AxisValue);
}

void AOPlayerCharacter::MoveLeftRight(float AxisValue)
{
    AddMovementInput(GetActorRightVector() * AxisValue);
}

void AOPlayerCharacter::LookUpDown(float AxisValue)
{
    AddControllerPitchInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AOPlayerCharacter::LookLeftRight(float AxisValue)
{
    AddControllerYawInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AOPlayerCharacter::Shoot()
{
    if (Gun) {
        Gun->PullTrigger();
    }
}

void AOPlayerCharacter::StartRunning()
{
    GetCharacterMovement()->MaxWalkSpeed = RunningSpeed;
}

void AOPlayerCharacter::StopRunning()
{
    GetCharacterMovement()->MaxWalkSpeed = WalkingSpeed;
}

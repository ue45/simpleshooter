// Copyright (C) 2021 Molfar Games. All rights reserved.

#pragma once

#include "Logging/LogMacros.h"

DECLARE_LOG_CATEGORY_EXTERN(LogMGTemp, All, All);
DECLARE_LOG_CATEGORY_EXTERN(LogMGWeapon, All, All);

// Copyright (C) 2021 Molfar Games. All rights reserved.

#include "Items/MGPickableActor.h"
#include "MGLog.h"

#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"

AMGPickableActor::AMGPickableActor()
{
    GetMeshComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
}

void AMGPickableActor::PostInitializeComponents()
{
    Super::PostInitializeComponents();

    if (RespawnDelayMin < 0) {
        UE_LOG(
            LogMGWeapon, Warning, TEXT("Incorrect RespawnDelayMin (%f) for '%s'. Resetting to zero."), RespawnDelayMin,
            *GetName());
        RespawnDelayMin = 0;
    }

    if (RespawnDelayMax < 0 || RespawnDelayMax < RespawnDelayMin) {
        UE_LOG(
            LogMGWeapon, Warning, TEXT("Incorrect RespawnDelayMax (%f) for '%s'. Resetting to RespawnDelayMin"),
            RespawnDelayMax, *GetName());
        RespawnDelayMax = RespawnDelayMin;
    }
}

void AMGPickableActor::BeginPlay()
{
    Super::BeginPlay();

    if (bAutoSpawn) {
        RespawnPickup();
    }
}

bool AMGPickableActor::IsAutoSpawnable() const
{
    return bAutoSpawn;
}

bool AMGPickableActor::IsRespawnable() const
{
    return bIsRespawnable;
}

float AMGPickableActor::GetRespawnDelayMin() const
{
    return RespawnDelayMin;
}

float AMGPickableActor::GetRespawnDelayMax() const
{
    return RespawnDelayMax;
}

bool AMGPickableActor::IsInstantlyRespawnable() const
{
    return bIsRespawnable && FMath::IsNearlyZero(RespawnDelayMin) && FMath::IsNearlyZero(RespawnDelayMax);
}

bool AMGPickableActor::IsActive() const
{
    return bIsActive;
}

void AMGPickableActor::OnUsed(APawn *InstigatorPawn)
{
    Super::OnUsed(InstigatorPawn);

    UGameplayStatics::PlaySoundAtLocation(this, PickupSound, GetActorLocation());

    bIsActive = false;

    SetMeshVisibility(false);
    OnPickedUp();

    if (bIsRespawnable) {
        FTimerHandle RespawnTimerHandle;
        float        RespawnDelay = FMath::RandRange(RespawnDelayMin, RespawnDelayMax);
        AActor::GetWorld()->GetTimerManager().SetTimer(
            RespawnTimerHandle, this, &AMGPickableActor::RespawnPickup, RespawnDelay, false);
    } else {
        Destroy();
    }
}

void AMGPickableActor::SpawnPickup()
{
    if (!bIsActive) {
        UStaticMeshComponent *Mesh = GetMeshComponent();
        if (Mesh) {
            Mesh->SetVisibility(true);
            Mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
        } else {
            UE_LOG(LogMGWeapon, Warning, TEXT("The pickable '%s' has no static mesh component attached."), *GetName());
        }

        bIsActive = true;
        OnSpawned();
    }
}

void AMGPickableActor::RespawnPickup()
{
    if (bIsRespawnable) {
        SpawnPickup();
    }
}

void AMGPickableActor::OnPickedUp()
{
}

void AMGPickableActor::OnSpawned()
{
}

void AMGPickableActor::SetMeshVisibility(bool bIsMeshVisible)
{
    UStaticMeshComponent *Mesh = GetMeshComponent();
    if (!Mesh) {
        UE_LOG(LogMGWeapon, Warning, TEXT("The pickable '%s' has no static mesh component attached."), *GetName());
    }

    Mesh->SetVisibility(bIsMeshVisible);
    // TODO: MeshComp->SetSimulatePhysics(bIsMeshVisible);
    Mesh->SetCollisionEnabled(bIsMeshVisible ? ECollisionEnabled::QueryAndPhysics : ECollisionEnabled::NoCollision);
}

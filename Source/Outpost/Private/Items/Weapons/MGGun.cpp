// Copyright (C) 2021 Molfar Games. All rights reserved.

#include "Items/Weapons/MGGun.h"
#include "MGLog.h"

#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"

// Useful links:
// 
// 1. What are the differences between the effective firing range and the maximum firing range in guns?
//    https://www.quora.com/What-are-the-differences-between-the-effective-firing-range-and-the-maximum-firing-range-in-guns
// 2. All official U.S. DoD military terms, and their definitions.
//    https://www.militaryfactory.com/dictionary/military-dictionary.php
// 3. Physics of firearms.
//    https://en.wikipedia.org/wiki/Physics_of_firearms
// 4. How Do Bullets Work in Video Games?
//    https://medium.com/@3stan/how-do-bullets-work-in-video-games-d153f1e496a8
// 5. Table of handgun and rifle cartridges
//    https://en.wikipedia.org/wiki/Table_of_handgun_and_rifle_cartridges

AMGGun::AMGGun()
{
	MeshComp              = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Weapon Mesh"));
	MaximumRange          = DefaultMaximumRange;
	MaximumEffectiveRange = DefaultMaximumEffectiveRange;
	Damage                = DefaultDamage;
	AmmoCapacity          = DefaultAmmoCapacity;
	GunState              = EMGGunState::Idle;

	SetRootComponent(MeshComp);
}

void AMGGun::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	ValidateAndRepair();
}

void AMGGun::ValidateAndRepair()
{
	if (MaximumRange < MinimumMaximumRange)
	{
		UE_LOG(LogMGWeapon, Warning,
		       TEXT("'%s' has incorrect MaximumRange of %f. The minimal acceptable range is %f. "
			       "Resetting it to default (%f)."),
		       *GetName(), MaximumRange, MinimumMaximumRange, DefaultMaximumRange);
		MaximumRange = DefaultMaximumRange;
	}

	if (MaximumEffectiveRange > MaximumRange)
	{
		UE_LOG(LogMGWeapon, Warning, TEXT("'%s' has incorrect MaximumEffectiveRange of %f, "
			       "which exceeds MaximumRange of %f. Resseting it to the MaximumRange."),
		       *GetName(), MaximumEffectiveRange, MaximumRange);
		MaximumEffectiveRange = MaximumRange;
	}
	else if (MaximumEffectiveRange < MinimumMaximumRange)
	{
		UE_LOG(LogMGWeapon, Warning, TEXT("'%s' has incorrect MaximumEffectiveRange of %f. It is too low. "
			       "The minimal acceptable range is %f. Resseting it to the minimal range."),
		       *GetName(), MaximumEffectiveRange, MinimumMaximumRange);
		MaximumEffectiveRange = MinimumMaximumRange;
	}

	if (FMath::IsNearlyZero(Damage))
	{
		UE_LOG(LogMGWeapon, Warning, TEXT("'%s' has incorrect Damage of %f. Resetting it to default (%f)."),
		       *GetName(), Damage, DefaultDamage);
		Damage = DefaultDamage;
	}

	if (ModelName.IsEmptyOrWhitespace())
	{
		UE_LOG(LogMGWeapon, Warning, TEXT("'%s' has blank gun ModelName."), *GetName());
	}

	if (ModelDescription.IsEmptyOrWhitespace())
	{
		UE_LOG(LogMGWeapon, Warning, TEXT("'%s' has blank gun ModelDescription"), *GetName());
	}

	AmmoAmount = HasInfiniteAmmo() ? InfiniteAmmo : AmmoCapacity;
}

void AMGGun::Fire()
{
	if (GunState == EMGGunState::Reloading)
	{
		return;
	}
	
	if (!HasAmmo())
	{
		UGameplayStatics::PlaySoundAtLocation(this, OutOfAmmoSound, GetActorLocation());

		return;
	}

	GunState = EMGGunState::Firing;

	RemoveAmmo(1);

	UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, MeshComp, MuzzleSocketName);
	UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());

	GunState = EMGGunState::Idle;
}

// ----------------------------------------------------------
// Ammo management
// ----------------------------------------------------------

int32 AMGGun::GetCurrentAmmo() const
{
	return AmmoAmount;
}

bool AMGGun::HasInfiniteAmmo() const
{
	return AmmoCapacity <= 0;
}

bool AMGGun::IsFullyLoaded() const
{
	return HasInfiniteAmmo() || AmmoAmount == AmmoCapacity;
}

bool AMGGun::HasAmmo() const
{
	return AmmoAmount != 0;
}

bool AMGGun::HasNoAmmo() const
{
	return AmmoAmount == 0;
}

void AMGGun::RemoveAmmo(const int32 Amount)
{
	if (Amount <= 0)
	{
		UE_LOG(LogMGWeapon, Warning, TEXT("Tried to remove %d ammo from '%s'. Ignoring."), Amount, *GetName());

		return;
	}

	if (!HasInfiniteAmmo())
	{
		AmmoAmount = FMath::Clamp(AmmoAmount - Amount, 0, AmmoCapacity);
	}
}

void AMGGun::InsertAmmo(const int32 Amount)
{
	if (Amount <= 0)
	{
		UE_LOG(LogMGWeapon, Warning, TEXT("Tried to insert %d ammo to the '%s'. Ignoring."), Amount,
		       *GetName());

		return;
	}

	if (!HasInfiniteAmmo())
	{
		AmmoAmount = FMath::Clamp(AmmoAmount + Amount, 0, AmmoCapacity);
	}
}

int32 AMGGun::Reload(const int32 Amount)
{
	if (Amount < 0)
	{
		UE_LOG(LogMGWeapon, Warning, TEXT("Tried to reload '%s' with %d amount of ammo. Ignoring."), *GetName(),
		       Amount);

		return 0;
	}

	if (IsFullyLoaded() || Amount == 0)
	{
		return Amount;
	}

	if (GunState != EMGGunState::Idle)
	{
		return Amount;
	}

	GunState = EMGGunState::Reloading;

	// The ammo amount needed to fully reload the gun
	const int32 AmmoNeeded    = AmmoCapacity - AmmoAmount;
	const int32 AmmoAvailable = AmmoNeeded <= Amount ? AmmoNeeded : Amount;
	const int32 AmmoRemaining = Amount - AmmoAvailable;
	InsertAmmo(AmmoAvailable);

	UGameplayStatics::PlaySoundAtLocation(this, ReloadSound, GetActorLocation());
	// TODO: Play reload animation

	GunState = EMGGunState::Idle;

	return AmmoRemaining;
}

FText AMGGun::GetModelName() const
{
	return ModelName;
}

FText AMGGun::GetModelDescription() const
{
	return ModelDescription;
}

// ----------------------------------------------------------
// Capabilities
// ----------------------------------------------------------

int32 AMGGun::GetAmmoCapacity() const
{
	return AmmoCapacity;
}

float AMGGun::GetDamage() const
{
	return Damage;
}

float AMGGun::GetMaximumRange() const
{
	return MaximumRange;
}

float AMGGun::GetMaximumEffectiveRange() const
{
	return MaximumEffectiveRange;
}

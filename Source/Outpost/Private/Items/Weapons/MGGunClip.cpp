// Copyright (C) 2021 Molfar Games. All rights reserved.

#include "Items/Weapons/MGGunClip.h"
#include "MGLog.h"

static constexpr int32 DefaultCapacity = 20;

AMGGunClip::AMGGunClip()
{
	Capacity = DefaultCapacity;
	Ammo     = Capacity;
}

void AMGGunClip::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (Capacity <= 0)
	{
		UE_LOG(LogMGWeapon, Warning, TEXT("'%s' has incorrect gun clip capacity (%d). Resetting to default"),
		       *GetName(), Capacity);
		Capacity = DefaultCapacity;
	}

	if (Ammo < 0 || Ammo > Capacity)
	{
		UE_LOG(LogMGWeapon, Warning,
		       TEXT("'%s' has incorrect gun clip ammo amount (%d). Resetting to capacity."),
		       *GetName(), Ammo);
		Ammo = Capacity;
	}
}


int32 AMGGunClip::GetCapacity() const
{
	return Capacity;
}

int32 AMGGunClip::GetAmmo() const
{
	return Ammo;
}

bool AMGGunClip::HasAmmo() const
{
	return Ammo > 0;
}

void AMGGunClip::UseAmmo(int32 Amount)
{
	if (Ammo <= 0)
	{
		return;
	}

	if (Amount <= 0)
	{
		UE_LOG(LogMGWeapon, Warning, TEXT("Tried to get %d ammo from '%s'. Ignoring."), Amount, *GetName());

		return;
	}

	const int32 NewAmmo = Ammo - Amount;

	Ammo = NewAmmo > 0 ? NewAmmo : 0;
}

void AMGGunClip::GiveAmmo(int32 Amount)
{
	if (Amount <= 0)
	{
		UE_LOG(LogMGWeapon, Warning, TEXT("Tried to give %d ammo to '%s'. Ignoring."), Amount, *GetName());

		return;
	}

	const int32 NewAmmo = Ammo + Amount;

	Ammo = NewAmmo <= Capacity ? NewAmmo : Capacity;
}

// Copyright (C) 2021 Molfar Games. All rights reserved.

#include "Items/MGUsableActor.h"
#include "Components/StaticMeshComponent.h"

AMGUsableActor::AMGUsableActor()
{
    MeshComp      = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
    RootComponent = MeshComp;
}

void AMGUsableActor::OnGainFocus()
{
    // https://www.tomlooman.com/the-many-uses-of-custom-depth-in-unreal-4/
    // https://www.tomlooman.com/multi-color-outline-post-process-in-unreal-engine-4/
    // https://www.youtube.com/watch?v=JH07z9Ap1hk
    // https://www.youtube.com/watch?v=ZeYlal1nYrQ
    // https://www.youtube.com/watch?v=gWbfKTZjvK0
    // https://www.youtube.com/watch?v=PiQ_JLJKi0M
    MeshComp->SetRenderCustomDepth(true);
}

void AMGUsableActor::OnLostFocus()
{
    MeshComp->SetRenderCustomDepth(false);
}

void AMGUsableActor::OnUsed(APawn *InstigatorPawn)
{
}

# Simple Shooter aka Outpost

Playing with Unreal Engine 4.

## Cloning the repository

The repository uses Git LFS so make sure you have a client installed before
cloning the project.

## Coding Conventions

We follow [Epic Games Coding Standard][] with one exception: set your tab size to 8 characters.

### Tools

Use `clang-format` to format the code.

### Type Naming

All game types should include prefix `MG`, e.g. `UMGCarryObjectComponent`,
`AMGCharacter`, etc.

### Consecutive Assignments

Align consecutive assignments:

```c++
AMGGun::AMGGun()
{
        MeshComp     = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Weapon Mesh"));
        MaxRange     = DefaultMaxRange;
        Damage       = DefaultDamage;
        AmmoCapacity = DefaultAmmoCapacity;
}
```

### Access Modifiers

When declaring a class, first specify its private, then protected, then public members so that one could easily find the needed block by scrolling the file and looking at the left side of the code:

```c++
UCLASS(ABSTRACT)
class OUTPOST_API AMGGun : public AActor
{
        GENERATED_BODY()

        // The current amount of ammo in the gun.
        int32 AmmoAmount;
	
protected:
        AMGGun();
	
public:
        /// Get the current amount of ammo in the gun.
        /// The negative value means the gun has infinite ammo.
        UFUNCTION(BlueprintPure, Category = "Ammo")
        int32 GetCurrentAmmo() const;
}
```

Never mix access modifiers as it makes the navigation less convenient, i.e. the following is prohibited:

```c++
UCLASS(ABSTRACT)
class OUTPOST_API AMGGun : public AActor
{
        GENERATED_BODY()

        int32 AmmoAmount;
	
protected:
        AMGGun();
	
private: // WRONG!
        int32 GunCapacity;
	
public:
        UFUNCTION(BlueprintPure, Category = "Ammo")
        int32 GetCurrentAmmo() const;
}
```

### Logical Groups

Use comment blocks to visually separate logical groups of class members:

```c++
// ----------------------------------------------------------
// Capabilities
// ----------------------------------------------------------

UPROPERTY(EditDefaultsOnly, Category = "Capabilities")
float MaxRange;

UPROPERTY(EditDefaultsOnly, Category = "Capabilities")
float Damage;

UPROPERTY(EditDefaultsOnly, Category = "Capabilities")
int32 AmmoCapacity;

// ----------------------------------------------------------
// Effects
// ----------------------------------------------------------

UPROPERTY(EditDefaultsOnly, Category = "Effects")
UParticleSystem *MuzzleEffect;

// ----------------------------------------------------------
// Sounds
// ----------------------------------------------------------

UPROPERTY(EditDefaultsOnly, Category = "Sounds")
USoundBase *FireSound;

UPROPERTY(EditDefaultsOnly, Category = "Sounds")
USoundBase *OutOfAmmoSound;

UPROPERTY(EditDefaultsOnly, Category = "Sounds")
USoundBase *ReloadSound;

// ----------------------------------------------------------
// Components
// ----------------------------------------------------------

UPROPERTY(VisibleDefaultsOnly, Category = "Components")
USkeletalMeshComponent *MeshComp;
```

### Commenting Code

Always provide documentation comments for type definitions; public and protected class members.

[Epic Games Coding Standard]: https://docs.unrealengine.com/4.26/en-US/ProductionPipelines/DevelopmentSetup/CodingStandard/
